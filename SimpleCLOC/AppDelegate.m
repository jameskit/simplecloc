//
//  AppDelegate.m
//  SimpleCLOC
//
//  Created by James on 2/22/2558 BE.
//  Copyright (c) 2558 James. All rights reserved.
//

#import "AppDelegate.h"
#import "Drag.h"

@interface AppDelegate ()

@end

/*!
 * @typedef Find Charector Mode
 * @brief A list of state to find char.
 * @constant NORMAL state to find next char.
 * @constant COMMENT_IN state found char start comment.
 * @constant COMMENT_LINE state found inline comment.
 * @constant COMMENT_STAR state found start block comment.
 * @constant COMMENT_STAR_OUT state found end block comment.
 * @constant STRING state found string symbol.
 * @constant STRING_SPECIAL state check type of string.
 * @constant PREPROCESSING state to continue check next char.
 * @constant CHAR state to check type of char.
 * @constant CHAR_SPECIAL state found special typr of char.
 * @constant NEW_LINE state found newline char.
 */

typedef enum {
    NORMAL = 0,
    COMMENT_IN = 1,
    COMMENT_LINE = 2,
    COMMENT_STAR = 3,
    COMMENT_STAR_OUT = 4,
    STRING = 5,
    STRING_SPECIAL = 6,
    PREPROCESSING = 7,
    CHAR = 8,
    CHAR_SPECIAL = 9,
    NEW_LINE = 10
} mode;

@implementation AppDelegate

/// Declaration code line count.
long semicolon = 0;

/// Declaration comment line count.
long comment_physical = 0;

/// Declaration blank line count.
long blank_line = 0;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    /// Override point for customization after application launch.
    
    /// Register drag event to notification center.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dragNotification:)
                                                 name:@"dragNotification"
                                               object:nil];
}

/*!
 * @discussion Called when user drop a file in aplication window.
 */

- (void)dragNotification:(NSNotification *)notification {
    
    /// Hide explanation label.
    _explanationLabel.hidden = YES;
    
    /// Read system preference.
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    /// Find a filenames user selected.
    NSArray *array = [prefs objectForKey:@"fileNames"];
    
    /// Update total file to label.
    [_label setStringValue:[NSString stringWithFormat:@"Total Files : %lu",(unsigned long)array.count]];
}

/*!
 * @discussion A really simple way to calculate count line of code.
 * @param pathName An NSString a file path url.
 */

- (void)CLOC:(NSString*)pathName {
    
    /// Get path from filename.
    NSURL *url = [NSURL fileURLWithPath:pathName];
    
    /// Read data from path.
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    /// Convert raw data to text.
    NSString *txtStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    /// Set start state to NORMAL.
    int state = NORMAL;
    
    /// Reset brace count.
    long brace = 0;
    
    /// Reset comment line count.
    long comment_logical = 0;
    
    /// Reset parenthesis open count.
    long parenthesis_opened = 0;
    
    /// Reset preprocessing count.
    long preprocessing =  0;
    
    char message;
    
    /// Get one by one char in string.
    for(int i=0; i< txtStr.length; i++) {
        message = [txtStr characterAtIndex:i];
        
        if ((state!=STRING)&&(state!=CHAR)
            &&(state!=STRING_SPECIAL)&&(state!=CHAR_SPECIAL)
            &&(state!=PREPROCESSING)
            &&(state!=COMMENT_LINE)&&(state!=COMMENT_STAR)) {
            if (message==';') semicolon++;
            if (message=='{') brace++;
            if (message=='(') parenthesis_opened++;
        }
        
        switch (state) {
                
            /// NORMAL state to find next char.
            case NORMAL:
                switch (message) {
                    case '/': state = COMMENT_IN;
                        break;
                    case '"': state = STRING;
                        break;
                    case '#': {preprocessing++; state = PREPROCESSING;}
                        break;
                    case '\'': state = CHAR;
                        break;
                    case '\n': {state = NEW_LINE;}
                        break;
                    default: state = NORMAL;
                }
                break;
            
            /// PREPROCESSING state to continue check next char.
            case PREPROCESSING:
                switch (message) {
                    case '\n': state = NEW_LINE;
                        break;
                    default: state = PREPROCESSING;
                }
                break;
                
            /// NEW_LINE state found newline char.
            case NEW_LINE:
                switch (message) {
                    case '\n': { blank_line++; state = NORMAL;}
                        break;
                    case ' ': {state = NEW_LINE;}
                        break;
                    case '/': state = COMMENT_IN;
                        break;
                    default: state = NORMAL;
                }
                break;
            
            /// COMMENT_IN state found char start comment.
            case COMMENT_IN:
                switch (message) {
                    case '/': state = COMMENT_LINE;
                        break;
                    case '*': state = COMMENT_STAR;
                        break;
                    default: state = NORMAL;
                }
                break;
                
            /// COMMENT_LINE state found inline comment.
            case COMMENT_LINE:
                switch (message) {
                    case '\n': {comment_logical++; comment_physical++; state = NORMAL;}
                        break;
                    default: state = COMMENT_LINE;
                }
                break;
            
            /// COMMENT_STAR state found start block comment.
            case COMMENT_STAR:
                switch (message) {
                    case '*': state = COMMENT_STAR_OUT;
                        break;
                    case '\n': comment_physical++;
                        break;
                    default: state = COMMENT_STAR;
                }
                break;
            
            /// COMMENT_STAR_OUT state found end block comment.
            case COMMENT_STAR_OUT:
                switch (message) {
                    case '/': {comment_logical++; comment_physical++; state = NORMAL;}
                        break;
                    case '\n': comment_physical++;
                        break;
                    case '*': state = COMMENT_STAR_OUT;
                        break;
                    default: state = COMMENT_STAR;
                }
                break;
                
            /// STRING state found string symbol.
            case STRING:
                switch (message) {
                    case '\\': state = STRING_SPECIAL;
                        break;
                    case '"': state = NORMAL;
                        break;
                    default: state = STRING;
                }
                break;
                
            /// STRING_SPECIAL state check type of string.
            case STRING_SPECIAL:
                switch (message) {
                    default: state = STRING;
                }
                break;
                
            /// CHAR state to check type of char.
            case CHAR:
                switch (message) {
                    case '\\': state = CHAR_SPECIAL;
                        break;
                    case '\'': state = NORMAL;
                        break;
                    default: state = CHAR;
                }
                break;
                
            /// CHAR_SPECIAL state found special typr of char.
            case CHAR_SPECIAL:
                switch (message) {
                    default: state = CHAR;
                }
                break;
                
            default: break;
        }
    }
    
    /// Update sum code lines to label.
    [_numcodelines setStringValue:[NSString stringWithFormat:@"%ld",(long)semicolon]];
    
    /// Update sum comment lines to label.
    [_numcommentlines setStringValue:[NSString stringWithFormat:@"%ld",(long)comment_physical]];
    
    /// Update sum blank lines to label.
    [_numblanklines setStringValue:[NSString stringWithFormat:@"%ld",(long)blank_line]];
    
    NSLog(@"%ld lines and %ld comments and %ld blank line", semicolon, comment_physical, blank_line);
}

/*!
 * @discussion Called when user click "Count line of code!" button.
 */

- (IBAction)start:(id)sender {
    
    /// Read system preference.
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    /// Find a filenames user selected.
    NSArray *array = [prefs objectForKey:@"fileNames"];
    
    /// No file exit.
    if (array.count==0) {
        return;
    }
    
    /// Send to Calulate Count line of code.
    for (int i= 0; i<array.count; i++) {
        
        NSString *name =  [array objectAtIndex:i];
        [self CLOC:name];
    }
    
    /// Clean file name list.
    [prefs setObject:nil forKey:@"fileNames"];
    
    /// Update Complete state to label.
    [_label setStringValue:@"Completed"];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    /// Called when the application is about to terminate. Save data if appropriate.
}

@end
