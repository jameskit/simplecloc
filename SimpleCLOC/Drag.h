//
//  Drag.h
//  SimpleCLOC
//
//  Created by James on 2/22/2558 BE.
//  Copyright (c) 2558 James. All rights reserved.
//

#import <AppKit/AppKit.h>

@interface Drag : NSImageView <NSDraggingSource, NSDraggingDestination, NSPasteboardItemDataProvider> {
    
    /// High application window state.
    BOOL highlight;
}

@property (nonatomic,strong) NSMutableArray *fileNames;

- (id)initWithCoder:(NSCoder *)aDecoder;

@end
