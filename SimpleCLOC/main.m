//
//  main.m
//  SimpleCLOC
//
//  Created by James on 2/22/2558 BE.
//  Copyright (c) 2558 James. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
