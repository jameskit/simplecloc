//
//  Drag.m
//  SimpleCLOC
//
//  Created by James on 2/22/2558 BE.
//  Copyright (c) 2558 James. All rights reserved.
//

#import "Drag.h"

@implementation Drag

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];

    if (self) {
        /// Resister dragged file type.
        [self registerForDraggedTypes:[NSArray arrayWithObject:NSFilenamesPboardType]];
    }
    
    return self;
}


- (void)pasteboard:(NSPasteboard *)sender item:(NSPasteboardItem *)item provideDataForType:(NSString *)type
{
    /// method called by pasteboard to support promised drag types.
    
}

- (NSDragOperation)draggingSession:(NSDraggingSession *)session sourceOperationMaskForDraggingContext:(NSDraggingContext)context {
    if ( context == NSDraggingContextWithinApplication ) {
        return NSDragOperationCopy;
    }
    else
        return NSDragOperationNone;
}

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    
    /// Called when drag file entered application window.
    
    NSLog(@"draggingEntered");
    
    /// Set draw highlight application window.
    highlight = YES;
    
    /// Call application window display update.
    [self setNeedsDisplay: YES];
    
    return NSDragOperationLink;
}


- (void)draggingExited:(id<NSDraggingInfo>)sender {
    
    /// Called when drag file exited application window.
    
    NSLog(@"draggingExited");
    
    /// Set draw highlight application window.
    highlight = NO;
    
    /// Call application window display update.
    [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
    
    [super drawRect:dirtyRect];
    
    /// Draw highlight style.
    if (highlight) {
        [[NSColor grayColor]set];
        [NSBezierPath setDefaultLineWidth:10];
        [NSBezierPath strokeRect:dirtyRect];
    }
}

- (BOOL)performDragOperation:(id < NSDraggingInfo >)sender {
    
    /// Reset draw highlight application window.
    highlight = NO;
    
    /// Call application window display update.
    [self setNeedsDisplay:YES];
    
    /// Check type of file name.
    NSMutableArray *draggedFilenames = [[sender draggingPasteboard] propertyListForType:NSFilenamesPboardType];
    if ([[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"txt"] ||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"cpp"]||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"c"] ||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"m"] ||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"h"] ||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"java"] ||
        [[[draggedFilenames objectAtIndex:0] pathExtension] isEqual:@"swift"]) {
        
        /// Set list file name.
        self.fileNames = draggedFilenames;
        
        return YES;
    }
    else {
        return NO;
    }
}

-(void)concludeDragOperation:(id<NSDraggingInfo>)sender{
    
    /// Read system preference.
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    /// Update filename list.
    [prefs setObject:self.fileNames forKey:@"fileNames"];
    
    /// Call update system preference.
    [prefs synchronize];
    
    NSLog(@"%@", self.fileNames);
    
    /// Send drag event to notification center.
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"dragNotification"
     object:self];
}

@end
