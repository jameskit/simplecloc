//
//  AppDelegate.h
//  SimpleCLOC
//
//  Created by James on 2/22/2558 BE.
//  Copyright (c) 2558 James. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSTextField *label;
@property (weak) IBOutlet NSTextField *explanationLabel;
@property (weak) IBOutlet NSTextField *numcodelines;
@property (weak) IBOutlet NSTextField *numcommentlines;
@property (weak) IBOutlet NSTextField *numblanklines;

/*!
 * @discussion A really simple way to calculate count line of code.
 * @param pathName An NSString a file path url.
 */
- (void)CLOC:(NSString*)pathName;

@end

